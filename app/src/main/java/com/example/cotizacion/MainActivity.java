package com.example.cotizacion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Cotizacion cotizacion=new Cotizacion();
    private EditText txtcliente;
    private Button btningresar;
    private  Button btnterminar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtcliente=(EditText) findViewById(R.id.txtcliente);
        btningresar=(Button) findViewById(R.id.btningresar);
        btnterminar=(Button) findViewById(R.id.btnterminar);

        btningresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cliente = txtcliente.getText().toString();
                if (cliente.matches("")){
                    Toast.makeText(MainActivity.this,"Falto capturar nombre",Toast.LENGTH_SHORT).show();
                } else{
                    Intent intent = new Intent(MainActivity.this,CotizacionActivity.class);
                    intent.putExtra("cliente",cliente);
                    Bundle objeto =new Bundle();
                    objeto.putSerializable("cotizacion",cotizacion);

                    intent.putExtras(objeto);

                    startActivity(intent);
                }
            }
        });
        btnterminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });




    }
}