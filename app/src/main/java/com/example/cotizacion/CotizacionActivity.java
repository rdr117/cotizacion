package com.example.cotizacion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


public class CotizacionActivity extends AppCompatActivity {
    private TextView lblFolio;
    private TextView txtCliente;
    private Cotizacion cotiza;
    private Button btnRegresar;
    private Button btnLimpiar;
    private Button btnCalcular;
    private EditText txtdescripcion;
    private EditText txtvalorauto;
    private EditText txtpagoinicial;
    private RadioButton rdb12;
    private RadioButton rdb24;
    private RadioButton rdb36;
    private RadioButton rdb48;
    private TextView lblpagomensual;
    private TextView lblpagoinicial;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);
        //
        lblFolio = (TextView)findViewById(R.id.txtFolio);
        txtCliente = (TextView) findViewById(R.id.txtCliente);
        //
        btnRegresar=(Button) findViewById(R.id.btnregresar);
        btnCalcular=(Button) findViewById(R.id.btncalcular);
        btnLimpiar=(Button) findViewById(R.id.btnlimpiar);
        //
        txtdescripcion=(EditText) findViewById(R.id.txtdescripcion);
        txtvalorauto=(EditText) findViewById(R.id.txtvalorauto);
        txtpagoinicial=(EditText) findViewById(R.id.txtpagoInicial);
        //
        rdb12=(RadioButton) findViewById(R.id.rdb12);
        rdb24=(RadioButton) findViewById(R.id.rdb24);
        rdb36=(RadioButton) findViewById(R.id.rdb36);
        rdb48=(RadioButton) findViewById(R.id.rdb48);
        //
        lblpagomensual = (TextView) findViewById(R.id.lblpagomensual);
        lblpagoinicial = (TextView) findViewById(R.id.lblpagoinicial);



        Bundle datos = getIntent().getExtras();
        txtCliente.setText(datos.getString("cliente"));

        cotiza = (Cotizacion) datos.getSerializable("cotizacion");
        lblFolio.setText(String.valueOf("Folio" + cotiza.getFolio()));

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtdescripcion.getText().toString().matches("")||
                        txtvalorauto.getText().toString().matches("")||
                        txtpagoinicial.getText().toString().matches("")){
                    Toast.makeText(CotizacionActivity.this,"falto algo",Toast.LENGTH_SHORT).show();

                    txtdescripcion.requestFocus();
                }else{
                    int plazos = 0;
                    float enganche=0;
                    float pagomensual=0.0f;

                    if(rdb12.isChecked()) {plazos= (int) 12f;}
                    if(rdb24.isChecked()) {plazos= (int) 24f;}
                    if(rdb36.isChecked()) {plazos= (int) 36f;}
                    if(rdb48.isChecked()) {plazos= (int) 48f;}



                    cotiza.setDescripcion(txtdescripcion.getText().toString());
                    cotiza.setValorAuto(Float.parseFloat(txtvalorauto.getText().toString()));
                    cotiza.setPorEnganche(Float.parseFloat(txtpagoinicial.getText().toString()));
                    cotiza.setPlazos(plazos);


                    enganche=cotiza.calcularPagoInicial();
                    pagomensual=cotiza.calcularPagoMensual();

                    lblpagomensual.setText("Pago Mensual$  " + String.valueOf(pagomensual));
                    lblpagoinicial.setText("Pago Inicial$ " +String.valueOf(enganche));
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtdescripcion.setText("");
                txtpagoinicial.setText("");
                txtvalorauto.setText("");
                lblpagomensual.setText("");
                lblpagoinicial.setText("");
            }
        });
    }
}
